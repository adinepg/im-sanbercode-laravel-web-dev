<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <!-- membuat form -->
    <form action="/welcome" method="post">
        @csrf
        <div style="margin-bottom: 20px;">
            <label for="firstname">First Name:</label> <br>
            <input type="text" id="firstname" name="firstname">
            <br>
        </div>
        <div style="margin-bottom: 20px;">
            <label for="lastname">Last Name:</label> <br>
            <input type="text" id="lastname" name="lastname">
            <br>
        </div>
        <div style="margin-bottom: 20px;">
            <label>Gender:</label> <br>
            <input type="radio" name="gender" value="male"> Male<br>
            <input type="radio" name="gender" value="female"> Female<br>
        </div>
        <div style="margin-bottom: 20px;">
            <label>Nationality:</label> <br>
            <select>
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
              </select>
            <br>
        </div>
        <div style="margin-bottom: 20px;">
            <label>Language Spoken:</label> <br>
            <input type="checkbox" name="gender" value="Bahasa Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" name="gender" value="English">English<br>
            <input type="checkbox" name="gender" value="Other">Other<br>
        </div>
        <div style="margin-bottom: 20px;">
            <label for="bio">Bio:</label> <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br>
        </div>
        <div style="margin-bottom: 20px;">
            <button type="submit">Sign Up</button>
        </div>
    </form>
</body>
</html>