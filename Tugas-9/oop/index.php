<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("Shaun"); // Membuat objek bar

echo "Name : ". $sheep->name . "<br>"; // "Shaun"
echo "legs : ". $sheep->legs . "<br>"; // 4
echo "cold blooded : ". $sheep->cold_blooded . "<br>"; // false

echo "<br>";

$kodok = new Frog("buduk");
echo "Name : ". $kodok->name . "<br>";
echo "legs : ". $kodok->legs . "<br>";
echo "cold blooded : ". $kodok->cold_blooded . "<br>";
echo "Jump : ";
echo $kodok->jump() . "<br>";

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->name . "<br>";
echo "legs : ". $sungokong->legs . "<br>";
echo "cold blooded : ". $sungokong->cold_blooded . "<br>";
echo "Yell : ";
$sungokong->yell();

?>