<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }
   
    public function welcome(Request $request)
    {
        $data = [
            'nama_depan' => $request->firstname,
            'nama_belakang' => $request->lastname
        ];

        return view('welcome', $data);
    }
}
